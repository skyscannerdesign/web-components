angular.module("flights", []).
controller('FlightsController', ['$scope', '$routeParams', 'DataService', function ($scope, $routeParams, DataService) {
    var module = $routeParams.module;
    var view = $routeParams.view;
    $scope.module = module;
    $scope.class = module;
    $scope.view = view;
    $scope.data = new DataService(module);
}]);