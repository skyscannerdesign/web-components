angular.module("cars", []).
controller('CarsController', ['$scope', '$http', function ($scope, $http) {
    $scope.class = "cars";
    $http.get('datasets/cars.json').success(function (data) {
        $scope.data = data;
    });
}]);