angular.module("hotels-results", []).
controller('HotelsController', ['$scope', '$http', function ($scope, $http) {
    $scope.class = "hotels";
    $http.get('datasets/hotels.json').success(function (data) {
        $scope.data = data;
    });
}]);