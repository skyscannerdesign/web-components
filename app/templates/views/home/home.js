angular.module("home", []).
controller('HomeController', ['$scope', '$http', function ($scope, $http) {
    $scope.class = "home";
    $http.get('datasets/home.json').success(function (data) {
        $scope.data = data;
    });
}]);