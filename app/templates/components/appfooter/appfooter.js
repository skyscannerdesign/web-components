app.directive("appfooter", function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: "components/appfooter/appfooter.html"
    }
});