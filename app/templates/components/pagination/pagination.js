app.directive("pagination", function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: "components/pagination/pagination.html"
    }
});