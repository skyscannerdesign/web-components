app.directive("resultcontrols", function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: "components/resultcontrols/resultcontrols.html"
    }
});