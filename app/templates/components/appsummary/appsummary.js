app.directive("appsummary", function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: "components/appsummary/appsummary.html"
    }
});