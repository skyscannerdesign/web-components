app.directive("hotelresult", function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: "components/hotelresult/hotelresult.html"
    }
});