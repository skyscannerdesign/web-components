#Work in progress!
##CSS Styleguide

When writing CSS, we use SASS (Syntactically Awesome Stylesheets) which allows us to extend the capabilities of CSS to make it behave more like a programming language. This allows us to use things like variables and functions which can help to streamline the development process and can be useful when breaking the design into smaller parts. All of this can then be compiled into a single minified file to save on resource. With great power comes great responsibility, which is why we have set out some house rules to how we approach writing CSS in Skyscanner.

---
 
###Naming
When naming components and classes, use hyphenated words like:
**.booking-panel{...}**


Please note: not every element needs a class name. Lean on the platform and use classes to scope components while relying on the underlying HTML elements to provide the necessary stucture without having to add classes to every element. Also, please do not use div's for every HTML element as HTML provides a plethora of elements that can be used for a wide variety of your data and data rendering needs (see [MDN HTML5 element list](https://developer.mozilla.org/en/docs/Web/Guide/HTML/HTML5/HTML5_element_list) for examples):)

```SASS
.component-name{
    h3{ // Use the scope of the selector and use standard HTML selectors where possible
        // This will output .component-name h3
    }
    p{
        // This will output .component-name p
    }
    ul{
        // This will target all unordered lists within .component-name
    }
}
```

We should also never specify page-level name spacing for our components as this restricts the thinking and limits usage. Each element should be able stand on its own when placed
 onto any page. Adopting this way of thinking and naming our components as such will ensure that we remain flexible and modular in our design thinking.

###CSS Selectors

CSS offers a range of highly useful selectors out of the box. Be sure to use these in favour of class names to save on byte-size and improve legibility. A full list of these can be seen on the [TutsPlus website](http://code.tutsplus.com/tutorials/the-30-css-selectors-you-must-memorize--net-16048). Please be aware of browser support and performance implications when using these. Some examples are:

```SASS
.component{
    > {
        // This will target the direct children of .component
    }
    &:hover{
        // This style will only be applied when the .component is hovered
    }   
    &:last-child{
        // This will target the last child item of the .component
    }
}
```

###CSS Writing style

####Selectors
When writing SASS/CSS, each selector should be on its own line with the open bracket on the same line as the last selector and the closing bracket on a new 
line. Typically selectors will be spaced with a single line break, but where there is a logical grouping of selectors, you might want to differentiate these with multiple line breaks. This will
 not affect the size of the outputted CSS but will help to improve readability.
 
```SASS
.selector1,
.selector2,
.selector3{
    property: value;
}
.next-selector{
    property: value;
}


.new-grouping-of-selector{
    property: value;
}
```
 
####Properties
When defining the CSS properties of a selector it is prefferred that this is structured alphabetically. This helps when scanning documents if you are looking for a particular 
property. If it is a border, it is near the top of the code block. If it is position, it will be nearer the bottom. For example:
```SASS
.my-selector{
    background: $gray-light;
    border: 1px solid $color-red;
    color: $color-red;
    margin: 0;
    padding: 0;
    position: absolute;
        top: 1em    // As these values relate directly to the position property above we change the order
        right: 2em; // In this case we nest the property in the CSS to reflect the relationship
}

###Components
```SASS
.component {
    header {
        // When component scope is kept quite small, it should be feasible to stick to 2nd level nesting
        color: red;
        
        &:hover, &:active{
            // An exception to this will be when working with element states. Here, we can use the '&' to append the state to the .element class
            color: blue;
        }
    }
}

// This will output:

.component header { color: red; }
.component header:hover, .component header:active { color: blue; }
```

###File structure

- resources/styles/
- includes
    - definitions.scss
    - elements.scss
    - mixins.scss
    - reset.scss

- generated
    - // DO NOT EDIT THESE FILE DIRECTLY AS THE ARE COMPILED AUTOMATICALLY FROM THE TEMPLATES/**/*.SCSS FILES.
    - directives.scss

*(The following used to import all of the styles from above directories which will output a single CSS file)*
- default.scss
- enhanced.scss

###Variables

Variables in SASS are written in the following format:

```SASS
// @variable :     value;
@light-blue :      #5BE2ED;
```

To use a variable value in your SASS file, all you do is:

```SASS
    h1{
        color: @color-brand;
    }
    
    //This will evaluate to:
    
    h1{
        color: #5BE2ED;
    }
```

The benefit of using a variable is that it promotes consistency by offering a number of presets that can be used, rather than having to recall what should be used in a given context. For variables to work, we need to have a standard format for defining variables. These should always be in the format:
 
 > @property-term: value;

###Colors

When writing out color values, use the HEX format of the color code. All colors should be defined in the colors.scss file. *Avoid adding new colors into componentname.scss 
files*. SASS allows us to modify colors through a range of functions. Some examples are *lighten* and *darken* which allow you to amend colors easily without the need to create 
new variations.

```SASS
*Wrong*
.input{
    border: 1px solid #FF4040;              // This becomes difficult to manage
}

*Right*
.input{
    border: 1px solid lighten($color-red, 5%);    // Using the core colors allows us to maintain color relations and improves consistency
}
```

###Typography

The standard web font stack for Skyscanner is "arial,helvetica,clean,sans-serif". Web fonts, font sizes and line heights will be available through SASS variables. These are 
all documented in typography.scss and will allow us to maintain visual consistency with our typography. 
 
```SASS

$font-size-smaller: 0.688em;     // equivalent to ~11px
$font-size-small: 0.813em;      // equivalent to ~13px
$font-size-base: 1em;           // Typically this equates to 16px
$font-size-large: 1.188em;      // equivalent to ~19px
$font-size-larger: 1.313em;     // equivalent to ~21px

$line-height-tight: ;
$line-height-normal: ;

```
###Mixins

SASS allows us to extend CSS to pass in functions and allows us to output tedious CSS by just using one line of CSS. For example:

```SASS

// This is a mixin to add a box shadow to an element
@mixin box-shadow($top: 0px, $left: 0px, $blur:1em, $spread: .2em, $color: $color-dark-charcoal, $inset: false) {
  @if $inset {
    -webkit-box-shadow:inset $top $left $blur $spread $color;
    -moz-box-shadow:inset $top $left $blur $spread $color;
    box-shadow:inset $top $left $blur $spread $color;
  } @else {
    -webkit-box-shadow: $top $left $blur $spread $color;
    -moz-box-shadow: $top $left $blur $spread $color;
    box-shadow: $top $left $blur $spread $color;
  }
}

// To use this in your SASS file, you just add
.homepage-boxes{
    @include box-shadow();
}
```

This is useful as it allows us to write our CSS much more quickly using standardised code. Rather than using custom values each time we create a box shadow, 
or digging through source code to find the de-facto box-shadow style, we can just call this one line which will give us a consistent look and feel and will allow us to update 
the code more easily. This also takes care of vendor prefixes, so if we are able to drop -webkit prefixes, this can be done from our central mixin which will then cascade out to
 all of the elements which use this. 

###Utility classes
Utility classes are small snippets of CSS which can be applied to the HTML to provide some basic styling. All utility classes begin with ".u-" and are followed by the utility 
name. For example

```HTML
<div class="u-group">
    // This utility will clear any floats within this group and maintain box height.
</div>
```
