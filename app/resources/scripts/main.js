var app = angular.module("app", ['ngRoute', 'components']);
app.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/:module?/:view?', {
                templateUrl: function(params) {
                    var home = 'home';
                    var module = params.module ? params.module : home ;
                    var view = params.view ? params.view : params.module;
                    var template = 'views/' + module + '/' + ( view ? view : module ) + '.html';
                    console.log(template);
                    return (template);
                },
                controller: 'ModulesController'
            })
            .otherwise({
                redirectTo: '/'
            });
    }
]);

app.controller('ModulesController', ['$scope', '$routeParams', 'DataService', function ($scope, $routeParams, DataService) {
        var module = view = 'home';
        if ($routeParams.module !== undefined) {
            module = $routeParams.module;
            view = $routeParams.view;
        }
        $scope.module = module;
        $scope.class = module;
        $scope.view = view;
        $scope.data = new DataService(module);
}]);

app.factory("DataService", ['$http', function($http) {
    function DataService(dataset) {
        this.load(dataset);
    }
    DataService.prototype = {
        setData: function(dataset) {
            angular.extend(this, dataset);
        },
        load: function(dataset) {
            var scope = this;
            var json = 'datasets/' + dataset + '.json'
            $http.get(json).success(function(data) {
                scope.setData(data);
            });
        },
        getName: function() {
            return (this.name);
        }
    };
    return (DataService);
}]);

app.run(function($http, $rootScope) {
    $http.get('datasets/app.json').success(function (data) {
        $rootScope.app = data;
        var language = 'datasets/language/' + $rootScope.app.lang + '.json';
        $http.get( language ).success(function (data) {
            $rootScope.strings = data;
        });
    });
});