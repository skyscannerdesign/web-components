angular.module("cars", []).
controller('CarsController', ['$scope', '$http', function ($scope, $http) {
    $scope.class = "cars";
    $http.get('datasets/cars.json').success(function (data) {
        $scope.data = data;
    });
}]);
angular.module("flights", []).
controller('FlightsController', ['$scope', '$routeParams', 'DataService', function ($scope, $routeParams, DataService) {
    var module = $routeParams.module;
    var view = $routeParams.view;
    $scope.module = module;
    $scope.class = module;
    $scope.view = view;
    $scope.data = new DataService(module);
}]);
angular.module("home", []).
controller('HomeController', ['$scope', '$http', function ($scope, $http) {
    $scope.class = "home";
    $http.get('datasets/home.json').success(function (data) {
        $scope.data = data;
    });
}]);
angular.module("hotels-results", []).
controller('HotelsController', ['$scope', '$http', function ($scope, $http) {
    $scope.class = "hotels";
    $http.get('datasets/hotels.json').success(function (data) {
        $scope.data = data;
    });
}]);
angular.module("hotels", []).
controller('HotelsController', ['$scope', '$http', function ($scope, $http) {
    $scope.class = "hotels";
    $http.get('datasets/hotels.json').success(function (data) {
        $scope.data = data;
    });
}]);