app.directive("appfooter", function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: "components/appfooter/appfooter.html"
    }
});
app.directive("appheader", function() {
    return {
        restrict: "E",
        replace: true,
        controller: "appHeaderController",
        templateUrl: "components/appheader/appheader.html"
    }
});
app.controller('appHeaderController', function($scope, $rootScope) {

});
app.directive("appsummary", function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: "components/appsummary/appsummary.html"
    }
});
app.directive("hotelresult", function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: "components/hotelresult/hotelresult.html"
    }
});
app.directive("pagination", function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: "components/pagination/pagination.html"
    }
});
app.directive("resultcontrols", function() {
    return {
        restrict: "E",
        replace: true,
        templateUrl: "components/resultcontrols/resultcontrols.html"
    }
});