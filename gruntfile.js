module.exports = function (grunt) {
    grunt.initConfig({
        clean: {
            generated: {
                src: ['app/resources/scripts/generated/*', 'app/resources/styles/generated/*']
            },
            www: {
                src: ['www/*']
            }
        },
        concat: {
            components: {
                src: ['app/templates/components/**/*.scss', 'app/templates/views/**/*.scss'],
                dest: 'app/resources/styles/generated/components.scss'
            },
            controllers: {
                src: ['app/templates/views/**/*.js'],
                dest: 'app/resources/scripts/generated/controllers.js'
            },
            directives: {
                src: ['app/templates/components/**/*.js'],
                dest: 'app/resources/scripts/generated/directives.js'
            },
            main: {
                src: ['app/resources/scripts/main.js', 'app/resources/scripts/generated/*.js'],
                dest: 'www/resources/scripts/main.js'
            }
        },
        connect: {
            server: {
                options: {
                    livereload: true,
                    hostname: '0.0.0.0',
                    keepalive: true,
                    port: 8080,
                    base: 'www'
                }
            }
        },
        copy: {
            html: {
                files: [{expand: true, flatten: false, cwd: 'app/', src: ['*.html'], dest: 'www/'}]
            },
            bower: {
                files: [{expand: true, flatten: true, cwd: 'bower_components', src: ['*/*.min.js', '*/*.min.js.map'], dest: 'app/resources/scripts/libs'}]
            },
            datasets: {
                files: [{expand: true, flatten: false, cwd: 'app/datasets', src: ['**/*.json'], dest: 'www/datasets'}]
            },
            libraries: {
                files: [{expand: true, flatten: false, cwd: 'app/resources/scripts/libs/', src: ['**/*.{js,map}'], dest: 'www/resources/scripts/libs/'}]
            },
            resources: {
                files: [
                    {expand: true, flatten: false, cwd: 'app/resources/fonts/', src: ['**/*.{ttf,woff,eof}'], dest: 'www/resources/fonts/'},
                    {expand: true, flatten: false, cwd: 'app/resources/images/', src: ['**/*.{png,svg,jpg,gif}'], dest: 'www/resources/images/'}
                ]
            }
        },
        ngtemplates:  {
            options: {
                module: 'components',
                standalone: true
            },
            app: {
                cwd: 'app/templates/',
                src: '**/*.html',
                dest: 'www/resources/scripts/templates.js'
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'www/resources/styles/default.css': 'app/resources/styles/default.scss'
                }
            }
        },
        watch: {
            options: {
                livereload: true,
                spawn: false
            },
            html: {
                files: ['app/*.html'],
                tasks: ['copy:html']
            },
            components: {
                files: ['app/templates/**/*.scss'],
                tasks: ['concat:components', 'sass']
            },
            datasets: {
                files: ['app/datasets/**/*.json'],
                tasks: ['copy:datasets']
            },
            templates: {
                files: ['app/templates/**/*.html'],
                tasks: ['ngtemplates']
            },
            scripts: {
                files: ['app/templates/**/*.js', 'app/resources/scripts/main.js'],
                tasks: ['concat:controllers', 'concat:directives', 'concat:main']
            },
            styles: {
                files: ['app/resources/styles/**/*.scss'],
                tasks: ['sass']
            },
            resources: {
                options: {
                    livereload: true
                },
                files: [ 'www/**/*']
            }
        }
    });

    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('build', ['clean', 'ngtemplates', 'concat', 'copy', 'sass']);

};