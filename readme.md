# Skyscanner Angular Web Components

> Angular components design prototype for the Skyscanner web application.

## Getting Started
Please ensure [Node.js](http://nodejs.org/) `~0.10.28` is installed along with [Grunt](http://gruntjs.com/getting-started) and [Bower](http://bower.io/) to manage the various required packages.

Once you have Node, Grunt (Grunt-CLI) and Bower installed open up a terminal and type the following to install the necessary Node modules:

```
> npm install
```

After the Node modules are installed type the following in the terminal to install the require Bower components:

```
> bower install
```

As the `gruntfile.js` is a work in progress (I've likely missed a few things) I'd suggest starting off with a good old `> grunt build` to ensure all the components, modules and controllers are up-to-date and building fine (to `www` which is ignore by Git) before kicking off a `> grunt connect` and `> grunt watch` (both of which are long-running processes) that provide a local development server running [LiveReload](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en) and a number of watch tasks also connected via LiveReload. Open your browser (I'd suggest Chrome
with it's excellent Web Inspector) and point your browser to [http://127.0.0.1:8080](http://127.0.0.1:8080) to get started. In order to test on devices I'd suggest using [GhostLab](http://vanamco.com/ghostlab/) and entering [your network IP address](https://kb.iu.edu/d/aapa) `> ipconfig` (on Windows, use `ifconfig` on Mac/Linux) in order for it to provide a URL accessible from other devices.

## Editing Components and Resources
All of the individual components are located in the `app/templates/components` folder and the related compound views (home, flights, hotels, car hire, etc) are located in `app/templates/views`. Each folder contains an HTML, CSS and JavaScript file that will automatically be combined at build time and provided to the application via the included `<script>` and `<link>` within the app scaffold (`app/index.html`).